# HTML-HMI

An example of electron app as a Human-Machine Interface

![screenshot](docs/html.png)
Make it run
````
$ git clone git@bitbucket.org:testtechsolutions/html-hmi.git

$ cd HTML-HMI

$ npm install

$ npm run postinstall

$ npm run start
````

Package it
````
$ npm install

$ npm run package
````
