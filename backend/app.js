const child_process = require("child_process");
const EventEmitter = require('events');
const {app, Menu, MenuItem, ipcMain} = require('electron');
const path = require('path');
const fs = require('fs')
const {SqlDb} = require('./database');
const {SimulatedConnection, AdsConnection, ModbusRTUConnection, ModbusTCPConnection, EthernetIPConnection, TagSubscriptions} = require('./connections');
const {MainWindow} = require('./windows');

class AppManager extends EventEmitter{
  constructor(){
    super();
    this.windows = {} //window classes
    this.connections = {}
    this.initDb = async ()=>{
      try{
        await this.db.runSql(`PRAGMA foreign_keys = True;`)
        const tables = {
          ConnectionTypes: `
        CREATE TABLE [ConnectionTypes] (
          "ID"	INTEGER PRIMARY KEY AUTOINCREMENT,
          [ConnectionType] TEXT NOT NULL
          );`,
          Connections: `
        CREATE TABLE "Connections" (
          "ID"	INTEGER PRIMARY KEY AUTOINCREMENT,
          "Connection"	TEXT NOT NULL,
          "ConnectionTypeID"	NUMERIC NOT NULL,
          "Description"	TEXT DEFAULT '',
          FOREIGN KEY("ConnectionTypeID") REFERENCES "ConnectionTypes"("ID")
        );`,
        ADSConnectionsParams : `
        CREATE TABLE "ADSConnectionsParams" (
          "ID"	INTEGER NOT NULL,
          "Host"	TEXT NOT NULL,
          "TargetAMS"	TEXT NOT NULL,
          "SourceAMS"	TEXT NOT NULL,
          "Port"	NUMERIC NOT NULL DEFAULT 851,
          "Pollrate"	NUMERIC NOT NULL,
          PRIMARY KEY("ID"),
          CONSTRAINT fk_connectionID
            FOREIGN KEY("ID")
            REFERENCES "Connections"("ID")
            ON DELETE CASCADE
        );`,
        EthernetIPConnectionsParams: `
        CREATE TABLE "EthernetIPConnectionsParams" (
          "ID"	INTEGER NOT NULL,
          "Host"	TEXT NOT NULL,
          "Port"	NUMERIC NOT NULL DEFAULT 44818,
          "Pollrate"	NUMERIC NOT NULL,
          PRIMARY KEY("ID"),
          CONSTRAINT fk_connectionID
            FOREIGN KEY("ID")
            REFERENCES "Connections"("ID")
            ON DELETE CASCADE
        );`,
        ModbusRTUConnectionsParams: `
        CREATE TABLE "ModbusRTUConnectionsParams" (
          "ID"	INTEGER NOT NULL,
          "Port"	TEXT NOT NULL DEFAULT "COM1",
          "StationID"	NUMERIC NOT NULL,
          "Pollrate"	NUMERIC NOT NULL,
          PRIMARY KEY("ID"),
          CONSTRAINT fk_connectionID
            FOREIGN KEY("ID")
            REFERENCES "Connections"("ID")
            ON DELETE CASCADE
        );`,
        GrblConnectionsParams: `
        CREATE TABLE "GrblConnectionsParams" (
          "ID"	INTEGER NOT NULL,
          "Port"	TEXT NOT NULL DEFAULT "COM1",
          "Pollrate"	NUMERIC NOT NULL,
          PRIMARY KEY("ID"),
          CONSTRAINT fk_connectionID
            FOREIGN KEY("ID")
            REFERENCES "Connections"("ID")
            ON DELETE CASCADE
        );`,
        ModbusTCPConnectionsParams: `
        CREATE TABLE "ModbusTCPConnectionsParams" (
          "ID"	INTEGER NOT NULL,
          "Host"	TEXT NOT NULL,
          "Port"	INTEGER NOT NULL DEFAULT 502,
          "StationID"	NUMERIC NOT NULL,
          "Pollrate"	NUMERIC NOT NULL,
          PRIMARY KEY("ID"),
          CONSTRAINT fk_connectionID
            FOREIGN KEY("ID")
            REFERENCES "Connections"("ID")
            ON DELETE CASCADE
        );`,
        DataTypes: `
        CREATE TABLE [DataTypes] (
          [DataType] TEXT NOT NULL,
          [Bytes] NUMERIC NOT NULL,
          [Modbus Registers] NUMERIC NOT NULL,
          [C Data Type] TEXT NOT NULL,
          PRIMARY KEY ([DataType])
          );`,
          Tags: `
        CREATE TABLE "Tags" (
          "ID"	INTEGER PRIMARY KEY AUTOINCREMENT,
          "Tag"	TEXT NOT NULL,
          "Description"	TEXT,
          "ConnectionID"	INTEGER NOT NULL,
          "DataType"	TEXT,
          "Value" TEXT,
          "Timestamp" INTEGER,
          FOREIGN KEY("DataType") REFERENCES "DataTypes"("DataType"),
          CONSTRAINT fk_connectionID
            FOREIGN KEY("ConnectionID")
            REFERENCES "Connections"("ID")
            ON DELETE CASCADE
        );`,
        ModbusTagParams: `
        CREATE TABLE "ModbusTagParams" (
          "ID"	NUMERIC NOT NULL,
          "Function" INTERGER,
          "Address"	NUMERIC NOT NULL,
          "Bit"	BIT DEFAULT 0,
          "WordSwapped"	BIT DEFAULT 0,
          "ByteSwapped" BIT DEFAULT 0,
          PRIMARY KEY("ID"),
          CONSTRAINT fk_TagID
            FOREIGN KEY("ID") REFERENCES "Tags"("ID")
            ON DELETE CASCADE
          );`,
        AdsTagParams:`
        CREATE TABLE "AdsTagParams" (
          "ID"	NUMERIC NOT NULL,
          "Address"	TEXT,
          PRIMARY KEY("ID"),
          CONSTRAINT fk_TagID
            FOREIGN KEY("ID") REFERENCES "Tags"("ID")
            ON DELETE CASCADE
        );`,       
        EthernetIPTagParams:`
        CREATE TABLE "EthernetIPTagParams" (
          "ID"	NUMERIC NOT NULL,
          "Address"	TEXT,
          PRIMARY KEY("ID"),
          CONSTRAINT fk_TagID
            FOREIGN KEY("ID") REFERENCES "Tags"("ID")
            ON DELETE CASCADE
          );`,
          TagSubscriptions: `
        CREATE TABLE "TagSubscriptions" (
          "ID"	INTEGER PRIMARY KEY AUTOINCREMENT
        );`,
        TagSubRelations: `
        CREATE TABLE "TagSubRelations" (
          "SubID"	INTEGER,
          "TagID" INTEGER,
          CONSTRAINT fk_subID
            FOREIGN KEY("SubID")
            REFERENCES "TagSubscriptions"("ID")
            ON DELETE CASCADE,
          CONSTRAINT fk_tagID
            FOREIGN KEY("TagID")
            REFERENCES "Tags"("ID")
            ON DELETE CASCADE
        );`}
        const views = {
          ModbusTags: `
        CREATE VIEW ModbusTags as SELECT *
          FROM [Tags] INNER JOIN ModbusTagParams ON 
          Tags.ID = ModbusTagParams.ID;`
        ,
        AdsTags: `
        CREATE VIEW AdsTags as SELECT *
          FROM [Tags] INNER JOIN AdsTagParams ON 
          Tags.ID = AdsTagParams.ID;`,
        EthernetIPTags: `
        CREATE VIEW EthernetIPTags as SELECT *
          FROM [Tags] INNER JOIN EthernetIPTagParams ON 
          Tags.ID = EthernetIPTagParams.ID;
          `,
        }

        for (let t in tables){
          await this.db.runSql(tables[t]).catch(err=>console.log(err))
        }
        for (let v in views){
          await this.db.runSql(views[v]).catch(err=>console.log(err))
        }
        await this.db.runSql(`INSERT INTO [ConnectionTypes] ([ID], [ConnectionType]) VALUES (?,?),(?,?),(?,?),(?,?),(?,?),(?,?);`,
          [1,"Local", 2,"ModbusTCP", 3,"ModbusRTU", 4,"EthernetIP", 5,"ADS",6,"GRBL"]);

        const dTypes = [["BOOL", 1, 1, "Boolean"],
                ["DINT", 4, 2, "int32_t"],
                ["INT", 2, 1, "int16_t"], 
                ["LINT", 8, 4, "int64_t"], 
                ["LREAL", 8, 4, "double"], 
                ["REAL", 4, 2, "float"],
                ["SINT", 1, 1, "int8_t"],
                ["STRING", 80, 40, "char[80]"], 
                ["UDINT", 4, 2, "uint32_t"],
                ["UINT", 2, 1, "uint16_t"],
                ["ULINT", 8, 4, "uint32_t"], 
                ["USINT", 1, 1, "uint8_t"]
                ]
        for (let d in dTypes){
          await this.db.runSql(`INSERT INTO [DataTypes] ([DataType], [Bytes], [Modbus Registers], [C Data Type]) VALUES (?,?,?,?);`, dTypes[d])
        } 

      
      this.emit('appDbReady')
      }
      catch(err){
        console.log(err);
      }

    }
    this.db = new SqlDb();
    this.initDb()
      .catch((err)=>{console.log(err)})
      
    this.tagSubscriptions = new TagSubscriptions(this);

    this.newWindow = async (params)=>{
      try{
        if (params.window == undefined){
          reject("Attempting to create a window without an ID");
        }
        params.appManager = this;
        const windowClass = new windowClasses[params.window](params)
        //const name = params.window
        this.windows[params.window] = windowClass
        return windowClass
        //windowClass.window.webContents.on("close", this.closeWindow(name))
        windowClass.window.webContents.on("dom-ready", ()=>{
          return windowClass})   
        }
        catch(err){
          console.log(`${err.stack}\n${err.message}`)
        }
      }
      
    this.updateTagValues = async (tagValues)=>{
      try{
        var tagSubs = {} //{subID: [tagId,tagId...]}
        //update all of the tags in the db
        for(let id in tagValues){
          let subResults = await this.db.execSql(`SELECT TagSubRelations.SubID, TagSubRelations.TagID FROM TagSubRelations WHERE TagSubRelations.TagID = ?;`, [id])
            .catch(err=>console.log('Tag sub lookup error', err))
          if(subResults.length){
            if(tagSubs[subResults[0].SubID] == undefined){
              tagSubs[subResults[0].SubID] = []
            }
            tagSubs[subResults[0].SubID].push(subResults[0].TagID)
          }
          await this.db.runSql(`UPDATE Tags SET Value= ?,  Timestamp= ? WHERE Tags.ID= ?;`, [tagValues[id].value, tagValues[id].timestamp, id])
            .catch(err=>console.log('Tag update error', err))
        }
        
        // all subs that contain tags from this connection were will alerted, the actual tags that have been updated will come in the signal
        for(let subId in tagSubs){
          this.emit('TagSubscriptionUpdate', {subId: Number(subId), tagIds: tagSubs[subId]})
        }
        //comes back from the connection or updateConnections() if connection disconnects
        //connections don't know or care about subIds
        //they have to be told only about the unique tags to poll
        //tagValues = [{TagID: 1, Value: 3.14159, Timestamp: 15005632123}, ...]
        //insert new values into this.db
        //query which Subscriptions are affected
        //generate an array of the updates for each subscription
        //update the tags in the central db
        
      }
      catch(err){
        console.log(err.message, err.stack)
      }
    }

    this.closeApp = ()=>{
      try{
        setTimeout(app.quit, 1000);
      }
      catch(err){
        console.log(err.message, err.stack)
      }
    }
    
    // Quit when all windows are closed.
    app.on('window-all-closed', this.closeApp)
    const windowClasses = {
      "MAIN": MainWindow
    }

    app.on('ready', ()=>{
      this.newWindow({window: "MAIN"})
      .catch(err=>consol.log(err.message, err.stack))
    }) //load the main window here
    
    this.closeWindow = (id)=>{
      if (this.windows[id]){
        delete this.windows[id]
      }
    }
    this.sendToWindow = (id, signal, payload)=>{
      if (this.windows[id]){
        this.windows[id].window.webContents.send(signal, payload);
      }
    }

    this.windowListenerCallback = (WindowName, callbackName, params)=>{
      //checks if the window exists and then calls the function
      if(this.windows[WindowName] !== undefined){
        this.windows[WindowName][callbackName](params)
      }
    }

    this.closeApp = ()=>{
      setTimeout(app.quit, 100);
    }

    this.closeDb = function(){
      this.db = undefined;
      this.emit('dbClosed', '')
    }
    this.openConnection = async (ID)=>{
      try{
        const conx_result = await this.db.execSql(`SELECT * FROM [Connections] WHERE [ID] = ${ID};`)
        if(conx_result.length){
          let conx_params = conx_result[0]
          let paramResults = []
          switch(conx_result[0].ConnectionTypeID){
            case 1:
              console.log(`Local connection doesn't need to connect`)
              break;
            case 2://ModbusTCP
              paramResults = await this.db.execSql(`SELECT * FROM [ModbusTCPConnectionsParams] WHERE [ID] = ${ID};`)
              if(!paramResults.length){throw Error(`Database error, connection parameters missing for ID ${ID}`)}
              conx_params = Object.assign(paramResults[0], conx_params);
              this.connections[ID] = new ModbusTCPConnection(this, conx_params)
              await this.connections[ID].connect();
              break;
            
            case 3://ModbusRTU
              paramResults = await this.db.execSql(`SELECT * FROM [ModbusRTUConnectionsParams] WHERE [ID] = ${ID};`)
              if(!paramResults.length){throw Error(`Database error, connection parameters missing for ID ${ID}`)}
              conx_params = Object.assign(paramResults[0], conx_params);
              this.connections[ID] = new ModbusRTUConnection(this, conx_params);          
              await this.connections[ID].connect();
              break;
            case 4://EthernetIP
              paramResults = await this.db.execSql(`SELECT * FROM [EthernetIPConnectionsParams] WHERE [ID] = ${ID};`)
              if(!paramResults.length){throw Error(`Database error, connection parameters missing for ID ${ID}`)}
              conx_params = Object.assign(paramResults[0], conx_params);
              this.connections[ID] = new EthernetIPConnection(this, conx_params)
              await this.connections[ID].connect();
              break;
            case 5://ADS
              paramResults = await this.db.execSql(`SELECT * FROM [ADSConnectionsParams] WHERE [ID] = ${ID};`)
              if(!paramResults.length){throw Error(`Database error, connection parameters missing for ID ${ID}`)}
              conx_params = Object.assign(paramResults[0], conx_params);
              this.connections[ID] = new AdsConnection(this, conx_params)
              await this.connections[ID].connect();
              break;
          }
        }
        else{
          console.log(`No connection ID of ${ID} to connect to.`)
        }
      }
      catch(err){
        console.log(err.stack, err.message)
      }
    }

    this.connectionConnected = async (id) =>{      
      console.log(`connection ID: ${id}, connected`)
      await this.tagSubscriptions.updateConnections([id])
      this.emit('TagSubscriptionChanged', id)
    }

    this.connectionDisconnected = async (id) =>{      
      console.log(`connection ID: ${id}, disconnected`)
      delete this.connections[id]
      await this.tagSubscriptions.updateConnections([id])
    }
    this.csvImport = async (filename)=>{
      try{
        let contents = await new Promise((res, rej)=>{fs.readFile(filename, 'utf8', function(err, contents) {
          if(err){rej(err)}
          res(contents);
          })
        })
        const lines = contents.split("\r\n")
        let connDef = [];
        for(let l=1;l<2; l++){
          connDef = lines[l].split(',')
          let res = await this.db.runSql(`INSERT INTO Connections (ID, Connection, ConnectionTypeID, Description) VALUES (?,?,?,?);`,
          [connDef[1], connDef[2], connDef[3], connDef[4]])
          res = await this.db.runSql(`INSERT INTO ModbusTCPConnectionsParams (ID, Host, Port, StationID, Pollrate) VALUES (?,?,?,?,?);`,
          [connDef[1], connDef[5], connDef[6], connDef[7], connDef[8]])
        }
        for(let l=3; l<35; l++){
          const tagDef = lines[l].split(',')
          //add some tags to the central db
          let res = await this.db.runSql(`INSERT INTO Tags (ID, Tag, Description, ConnectionID, DataType) VALUES (?,?,?,?,?);`, tagDef.slice(1,6))
          await this.db.runSql(`INSERT INTO ModbusTagParams (ID, Function, Address, Bit, Wordswapped, Byteswapped) VALUES (?,?,?,?,?,?);`, [res.lastID,...tagDef.slice(6,11)])
        }
          //create a connection object
        await this.openConnection(connDef[1])
        }
        catch(err){
          console.log(err)
        }

    }
    this.dbImport = async(filename)=>{
      //tags columns different between builder and here
      //Do we want to systematically add connections then tags into here 
      let filepath = path.join(__dirname, filename)

      let test = await this.db.runSql(`ATTACH DATABASE ? AS TestDB`, [filepath])
      //console.table(await this.db.execSql(`SELECT * FROM TestDB.Connections`))
      const tables = [
        "Connections",
        "ADSConnectionsParams",
        "EthernetIPConnectionsParams",
        "ModbusRTUConnectionsParams",
        "ModbusTCPConnectionsParams",
        "Tags",
        "ModbusTagParams",
        "AdsTagParams",
        "EthernetIPTagParams"]
      for(let t in tables){
        await this.db.runSql(`INSERT INTO main.${tables[t]} SELECT * FROM TestDB.${tables[t]}`)
      }
      let connRes = await this.db.execSql(`SELECT ID FROM Connections;`)
      for(let r in connRes){
        await this.openConnection(Number(connRes[r].ID))
      }
      //console.log(await this.db.execSql(`SELECT * FROM ModbusTCPConnectionsParams;`))
      //console.log('done')
      //console.log(await this.db.execSql(`SELECT * FROM Tags;`))

    }
    this.writeToConnection = async (tagWrite)=>{
      try{//tagWrite = {connection: "someConnection", tag: "someTag", value: "100.1"}
        let tagRes = await this.db.execSql(`
          SELECT Tags.ID AS TagID, Connections.ID AS ConnectionID
          FROM Tags INNER JOIN Connections ON Tags.ConnectionID = Connections.ID
          WHERE Connections.Connection = ? AND Tags.Tag = ?;`,
          [tagWrite.connection, tagWrite.tag])
        if(tagRes.length){
          if(tagRes.length > 1){
            throw Error(`Multiple Tag / Connection in the database with the names ${tagWrite.connection}:${tagWrite.tag}. Tag write would be ambiguous.`)
          }
          else{
            if(this.connections[tagRes[0].ConnectionID]){
              this.connections[tagRes[0].ConnectionID].tagWrites.push({id: tagRes[0].TagID, value: tagWrite.value})
            }
            else{
              throw(Error(`Connection: "${tagWrite.connection}" is not open.`))
            }
          }
        }
        else{
          throw Error(`Unknown Tag / Connection in the database with the names ${tagWrite.connection}:${tagWrite.tag}.`)
        }
      }
      catch(err){
        console.log(err)
      }
    }


    this.on('appDbReady',async (e)=>{
      //this.dbImport("NewConfig.db")
      this.dbImport("mill.db")
      //this.csvImport('./backend/MG.csv');
    })

      


    
    ipcMain.on('changeSubscription-window', (e, payload)=>{
      const sub = JSON.parse(payload)
      if(this.windows[sub.window].subId != undefined){
        this.windows[sub.window].changeSubscription(sub.tags)
      }
    })
    ipcMain.on('writeToConnection', (e, payload)=>{
      const tagWrite = JSON.parse(payload)
      this.writeToConnection(tagWrite)
    })
    ipcMain.on('requestTagIds-window', (e, payload)=>{
      const request = JSON.parse(payload);
      if(this.windows[request.window] != undefined){
        this.windows[request.window].tagIdRequest(request);
      }
    })
    ipcMain.on('requestPageDef', (e, payload)=>{
      const request = JSON.parse(payload);
      let pageData = fs.readFileSync(`./public/page_defs/${request.page}.json`);
      this.windows[request.window].pageDefLoad(pageData)
    })
  }
}





module.exports = {AppManager};



