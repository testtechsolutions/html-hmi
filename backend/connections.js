// create a tcp modbus client
const sqlite3 = require('sqlite3').verbose();
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline')
const parser = new Readline()
const Modbus = require('jsmodbus');
const { Controller: EthernetIPController,
  Tag: EthernetIPTag,
  TagGroup:EthernetIPTagGroup} = require("ethernet-ip"); //alias the name to be clear
const net = require('net');
const fs = require('fs');
const EventEmitter = require('events');
const ads = require('node-ads');
const {SqlDb} = require('./database')


class Connection extends EventEmitter{
  constructor(appManager, params){
    super();
    this.appManager = appManager;
    this.connected = false;
    this.id = params.ID;
    this.pollrate = params.Pollrate;
    this.pollLoop = null;
    this.tagWrites = [];
    this.dataTypes = {BOOL: {bytes: 1, registers: 1, c_type: "boolean",
                      getVal: (buf, offset)=>{return (0 != ((buf.readUInt8(offset) & 1)));},
                      setVal: (val)=>{return Buffer.from(new Uint8Array([0!=Number(val)]))}
                    },
                      DINT: {bytes: 4, registers: 2, c_type: "int32_t", 
                      getVal: (buf, offset)=>{return buf.readInt32LE(offset);},
                      setVal: (val)=>{const b=Buffer.alloc(4); b.writeInt32LE(val); return b;}
                    },
                      INT: {bytes: 2, registers: 1, c_type: "int16_t", 
                      getVal: (buf, offset)=>{return buf.readInt16LE(offset);},
                      setVal: (val)=>{const b=Buffer.alloc(2); b.writeInt16LE(val); return b;}
                    },
                      LINT: {bytes: 8, registers: 4, c_type: "int64_t", 
                      getVal: (buf, offset)=>{return buf.readBigInt64LE(offset);},//need Node V12
                      setVal: (val)=>{const b=Buffer.alloc(8); b.writeBigUInt64LE(val); return b;}//need Node V12
                    },
                      LREAL: {bytes: 8, registers: 4, c_type: "double", 
                      getVal: (buf, offset)=>{return buf.readDoubleLE(offset);},
                      setVal: (val)=>{const b=Buffer.alloc(8); b.writeDoubleLE(val); return b;}
                    },
                      REAL: {bytes: 4, registers: 2, c_type: "float", 
                      getVal: (buf, offset)=>{return buf.readFloatLE(offset);},
                      setVal: (val)=>{const b=Buffer.alloc(4); b.writeFloatLE(val); return b;}
                    },
                      SINT: {bytes: 1, registers: 1, c_type: "int8_t", 
                      getVal: (buf, offset)=>{return buf.readInt8(offset);},
                      setVal: (val)=>{const b=Buffer.alloc(1); b.writeInt8(val); return b;}
                    },
                      STRING: {bytes: 80, registers: 40, c_type: "char[80]", 
                      getVal: (buf, offset)=>{return buf.toString('utf8', offset, offset+80);},
                      setVal: (val)=>{const b=Buffer.alloc(80); b.write(String(val), 0, val.length, 'utf8'); return b;}
                    },
                      UDINT: {bytes: 4, registers: 2, c_type: "uint32_t", 
                      getVal: (buf, offset)=>{return buf.readUInt32LE(offset);},
                      setVal: (val)=>{const b=Buffer.alloc(4); b.writeUInt32LE(val); return b;}
                    },
                      UINT: {bytes: 2, registers: 1, c_type: "uint16_t", 
                      getVal: (buf, offset)=>{return buf.readUInt16LE(offset);},
                      setVal: (val)=>{const b=Buffer.alloc(2); b.writeUInt16LE(val); return b;}
                    },
                      ULINT: {bytes: 8, registers: 4, c_type: "uint64_t", 
                      getVal: (buf, offset)=>{return buf.readBigUInt64LE(offset);},//need Node V12
                      setVal: (val)=>{const b=Buffer.alloc(8); b.writeBigUInt64LE(val); return b;}//need Node V12
                    },
                      USINT: {bytes: 1, registers: 1, c_type: "uint8_t", 
                      getVal: (buf, offset)=>{return buf.readUInt8(offset);},
                      setVal: (val)=>{const b=Buffer.alloc(1); b.writeUInt8(val); return b;}
                    }
                  }
    
    this.db = new SqlDb() //used for internal class sorting, etc.
    this.tagSubscriptions = [] //array of tag ids. If connected the poll loop updates any tags here
    this.appManager.on('TagSubscriptionChanged', async (id)=>{this.updateSubscription(id)})
    this.updateSubscription = async (id)=>{
      if(this.id==id){
        this.tagSubscriptions = await this.appManager.tagSubscriptions.getSubscriptionTags(this.id);
        this.emit('tagSubUpdate');
      }
    }

    this.writeToTags = async ()=>
    {
      while(this.tagWrites.length)
      {
        let tagWrite = this.tagWrites.pop(0);
        await this.writeTag(tagWrite);
      }
    }

    this.writeTag = async (tagWrite)=>{
      //override for each connection
      try{
        throw(Error(`writeTag method not implented for this connection type`))
      }
      catch(err){
        console.log(err)
      }
    }

    this.updateTags = async ()=>{
      await this.writeToTags();
      try{
        throw(Error(`updateTags method not implented for this connection type`))
      }
      catch(err){
        console.log(err)
      }
    }

    this.on('connected', ()=>{
      this.appManager.connectionConnected(this.id)
      this.connected = true 
    if(params.Pollrate != null){
      this.pollLoop = setInterval(this.updateTags, params.Pollrate, this)
    }
    })
    this.on('disconnected', ()=>{
      this.appManager.connectionDisconnected(this.id);
      if(this.pollLoop){
        clearInterval(this.pollLoop)
      }
      this.pollLoop = null;
      this.connected = false})
  }
}

class SimulatedConnection extends Connection{
  constructor(appManager, params){
    super(appManager, params);
    this.connect = async ()=>{
      this.emit('connected', '');
    }
    
    this.disconnect = async ()=>{
      this.emit('disconnected', '');
    }
    this.updateTags = async ()=>{
      await this.writeToTags();
      const ts = Date.now()
      let tagUpdates = {}
      for(let t in this.tagSubscriptions){
        tagUpdates[this.tagSubscriptions[t]] = {value: Math.random() * 10.0, timestamp: ts}
      }
      if(Object.keys(tagUpdates).length){
        this.appManager.updateTagValues(tagUpdates)
      }
    }
  }
}

class AdsConnection extends Connection{
  constructor(appManager, params){
    super(appManager, params);
    this.host = params.Host,
    this.amsNetIdTarget = params.TargetAMS,
    this.amsNetIdSource = params.SourceAMS,
    this.amsPortTarget= params.Port,
    this.timeout= params.timeout || 500;
    this.client = undefined;
    this.adsDataType = {
      BOOL: ads.BOOL,
      DINT: ads.DINT,
      INT: ads.INT,
      LINT:ads.LINT,
      LREAL: ads.LREAL,
      REAL: ads.REAL,
      SINT: ads.SINT,
      STRING: ads.STRING,
      UDINT: ads.UDINT,
      UINT: ads.UINT,
      ULINT: ads.USINT,
      USINT: ads.USINT
    }
    this.pollgroups = [];
    this.on('tagSubUpdate', ()=>{this.optimizePollgroups()})
    this.clientReady = ()=>{
      if(!this.client){throw(Error("Ads client not connected"))}
    }
    const cls = this;
    this.connect = ()=>{
      return new Promise((resolve, reject)=>{
      this.client = ads.connect(
        {
          host: cls.host,
          amsNetIdTarget: cls.amsNetIdTarget,
          amsNetIdSource: cls.amsNetIdSource,
          amsPortTarget: cls.amsPortTarget,
          timeout: cls.timeout
        }, ()=>{
          cls.emit('connected');
          
            resolve(cls.client)
          })
      this.client.on('error',  function(error) {
        reject(error)})
      })
    }
    this.disconnect = ()=>{
      return new Promise((resolve, reject)=>{
      this.client.disconnect({}, ()=>{
            this.emit('disconnected')
            this.client = null
            resolve(this.client)
          })
      })
    }

    this.optimizePollgroups = async ()=>{
      /*[{symname: '.TESTBOOL',
        bytelength: ads.BOOL},
        {symname: '.TESTINT',
        bytelength: ads.UINT}] */
      const readArr = []
      for(let t=0; t<this.tagSubscriptions.length; t++){
        let tagId = this.tagSubscriptions[t];
        let res = await this.appManager.db.execSql(`
        SELECT Address, DataType FROM
        AdsTags WHERE ID = ?;`, [tagId]);
        if(res.length){
          readArr.push({symname: res[0].Address,
          bytelength: this.adsDataType[res[0].DataType],
          tagId: tagId,
          dataType: res[0].DataType})
        }
      }
      this.pollgroups = [readArr];
      
    }

    this.updateTags = async ()=>{
      await this.writeToTags();
      //read thes tagValues
      for(let g=0; g<this.pollgroups.length; g++){
        this.client.multiRead(
          this.pollgroups[g],
          (error, handles)=>{
              if (error) {
                  console.log(error)
              }
              else {
                let tagUpdates = {}
                for(let h in handles){
                  if (handles[h].err) {
                    console.log(`Error: ${handles[h].symname}; ${ads.ERRORS[handles[h].err]}`)
                  } 
                  else {
                    tagUpdates[handles[h].tagId] ={
                      value: handles[h].value,
                      timestamp: Date.now()}
                    
                  }
                }
                if(Object.keys(tagUpdates).length){
                  //tagUpdates = {"46": {value: 9.99, timestamp: 150320402}, "85": {value: 10.99, timestamp: 150320402}}
                  this.appManager.updateTagValues(tagUpdates)
                }
              }
          })
        }
    }

    this.writeTag = async (tagWrite)=>{
      //{id: 1, value: "100.1"}
      let cls = this
      try{
        let res = await this.appManager.db.execSql(`SELECT DataType, Address FROM AdsTags WHERE ID = ?;`, [tagWrite.id])
        if(!res.length){throw Error(`Tag Write Error: Cannot find tag ${tagWrite.id} database`)}
        cls.client.getHandles([{symname: res[0].Address}], function (error, handles) {
          if (error) {
              console.log(error)
          } else if (handles.err) {
              console.error(handles.err)
          } else {
            let h = handles[0];
            //check if datatype size matches?
            h.propname='value'
            h.bytelength = cls.adsDataType[res[0].DataType];
            h.value = tagWrite.value;
            cls.client.write(h, (err)=>{
              if(err){
                console.log(err)
              }
            }
            )
          }
        })
      }
      catch(err){
        console.log(err)
      }
    }

    this.close = ()=>{
      if(this.client){
        this.client.end(()=>{this.emit('disconnected')})

      }
    }
  }
}

class EthernetIPConnection extends Connection{
  constructor(appManager, params){
    super(appManager, params);
    this.slot = params.Slot || 0, //CPU slot
    this.host = params.Host,
    this.client = undefined;
    this.pollgroups = [];
    this.on('tagSubUpdate', ()=>{this.optimizePollgroups()})
    const cls = this;
    this.connect = async ()=>{
      try{
        this.client = new EthernetIPController();
        await this.client.connect(this.host, this.slot);
        this.emit('connected')
      }
      catch(err){
        console.log(`Trouble connecting to connection ID ${this.id}, EthernetIP controller @ ${this.host}. ${err}`)
      }
    }

    this.optimizePollgroups = async ()=>{
      /*[{symname: '.TESTBOOL',
        bytelength: ads.BOOL},
        {symname: '.TESTINT',
        bytelength: ads.UINT}] */
      const tagGroup = new EthernetIPTagGroup();
      for(let t=0; t<this.tagSubscriptions.length; t++){
        let tagId = this.tagSubscriptions[t];
        let res = await this.appManager.db.execSql(`
        SELECT Address, DataType FROM
        EthernetIPTags WHERE ID = ?;`, [tagId]);
        if(res.length){
          let tag = new EthernetIPTag(res[0].Address)
          tag.id = tagId;
          tag.dataType = res[0].DataType
          tagGroup.add(tag)
        }
      }
      this.pollgroups = [tagGroup];
      
    }

    this.updateTags = async ()=>{
      await this.writeToTags();
      //read the tagValues
      try{
        let tagUpdates = {}
        for(let g=0; g<this.pollgroups.length; g++){
          await this.client.readTagGroup(this.pollgroups[g]);
          let now = Date.now()
          this.pollgroups[g].forEach(tag => {
            tagUpdates[tag.id] ={
              value: tag.value,
              timestamp: now};
          });
        }
        if(Object.keys(tagUpdates).length){
          //tagUpdates = {"46": {value: 9.99, timestamp: 150320402}, "85": {value: 10.99, timestamp: 150320402}}
          this.appManager.updateTagValues(tagUpdates)
        }
      }
      catch(err){
        console.log(`Error in EthernetIP tag read of connection ${this.id}, ${err}`)
      }
    }

    this.writeTag = async (tagWrite)=>{
      //{id: 1, value: "100.1"}
      let cls = this
      try{
        let res = await this.appManager.db.execSql(`SELECT DataType, Address FROM EthernetIPTags WHERE ID = ?;`, [tagWrite.id])
        if(!res.length){throw Error(`Tag Write Error: Cannot find tag ${tagWrite.id} database`)}
        let tag = new EthernetIPTag(res[0].Address);
        await cls.client.readTag(tag); //reading a tag sets its datatype
        tag.value = tagWrite.value;
        cls.client.writeTag(tag)
      }
      catch(err){
        console.log(err)
      }
    }

    this.close = ()=>{
      if(this.client){
        this.client.destroy(()=>{this.emit('disconnected')})
        
      }
    }
  }
}

class ModbusConnection extends Connection{
  constructor(appManager, params){
    super(appManager, params);

    this.optimizePollgroups = async ()=>{
      const ts = Date.now();
      try{
        if(this.optimizingActive){
          setTimeout(this.optimizePollgroups, 2000); //comeback later
        }
        this.optimizingActive = true;
        const newPollgroups = []
        await this.db.runSql(`DELETE FROM [TagParams]`, [])//clear the table
        for (var idx in this.tagSubscriptions){
          let tagRow = await this.appManager.db.execSql(`SELECT * FROM [ModbusTags] WHERE ID = ?`, [this.tagSubscriptions[idx]])
          if(tagRow.length){
            await this.db.runSql(`INSERT INTO [TagParams] ([ID], [DataType], [Function], [Address], [EndAddress], [Bit], [WordSwapped], [ByteSwapped]) 
            VALUES (?,?,?,?,?,?,?,?);`,
            [tagRow[0].ID, tagRow[0]["DataType"], tagRow[0].Function, Number(tagRow[0].Address), Number(tagRow[0].Address) + this.dataTypes[tagRow[0]["DataType"]].registers-1,
            tagRow[0].Bit, tagRow[0].WordSwapped, tagRow[0].ByteSwapped])
          }
        } 
        //console.table(await this.db.execSql(`SELECT * FROM [TagParams];`,[]))
        await this.db.runSql(`DROP TABLE IF EXISTS PollGroups;`)
        await this.db.runSql(`
        CREATE TABLE IF NOT EXISTS "PollGroups" (
          "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          "Function"	NUMERIC NOT NULL,
          "Address"	NUMERIC,
          "Length"	NUMERIC
          )`
          );
        const MAX_COIL_READ = 2000;
        const MAX_REG_READ = 125;
        let res
        for (let func = 1; func < 3; func++){
          let start = 0
          let end = start+MAX_COIL_READ
          let keepGoing = true;
          while(keepGoing){
            res = await this.db.execSql(`SELECT MIN(Address) AS [Start] FROM TagParams WHERE [Function] = ? AND [Address] >= ?`,[func, start])
            keepGoing = res[0].Start !== null
            if(keepGoing){
              start = res[0].Start
              end = start+MAX_COIL_READ
              res = await this.db.execSql(`SELECT ID, Function, Address, EndAddress FROM TagParams WHERE [Function] = ? AND [Address] >= ? AND [EndAddress] < ? ORDER BY Address`,[func, start, end])
              if(res.length){
                let pg = await this.db.runSql(`INSERT INTO PollGroups (Function, Address, Length) VALUES (?,?,?)`, [res[0].Function, res[0].Address, res[res.length-1].EndAddress-res[0].Address+1])
                newPollgroups[pg.lastID] = {tags:[], function: res[0].Function, addr: res[0].Address, len: res[res.length-1].EndAddress-res[0].Address+1, data: null, timestamp: null}
                for (var row in res){
                  let tagRes = await this.db.execSql(`SELECT Address, DataType, Bit, WordSwapped, ByteSwapped FROM TagParams WHERE ID = ?`, [res[row].ID])
                  newPollgroups[pg.lastID].tags.push({
                    id:res[row].ID,
                    offset:(Number(tagRes[0].Address) -  Number(res[0].Address)), //bit offset = (tag address - pollgroup address) 
                    dataType: tagRes[0].DataType,
                    bit: 0,
                    wordSwapped: 0,
                    byteSwapped:  0})
                  await this.db.runSql(`UPDATE TagParams
                  SET Pollgroup = ? WHERE ID = ?; `, [pg.lastID, res[row].ID]) 
                }     
              }
              start = end;
            }
          }
        }
        for (let func = 3; func < 5; func++){
          let start = 0
          let end = start+MAX_REG_READ
          let keepGoing = true;
          while(keepGoing){
            res = await this.db.execSql(`SELECT MIN(Address) AS [Start] FROM TagParams WHERE [Function] = ? AND [Address] >= ?`,[func, start])
            keepGoing = res[0].Start !== null
            if(keepGoing){
              start = res[0].Start
              end = start+MAX_REG_READ
              res = await this.db.execSql(`SELECT ID, Function, Address, EndAddress FROM TagParams WHERE [Function] = ? AND [Address] >= ? AND [EndAddress] < ? ORDER BY Address`,[func, start, end])
              if(res.length){
                let pg = await this.db.runSql(`INSERT INTO PollGroups (Function, Address, Length) VALUES (?,?,?)`, [res[0].Function, res[0].Address, res[res.length-1].EndAddress-res[0].Address+1])
                newPollgroups[pg.lastID] = {tags:[], function: res[0].Function, addr: res[0].Address, len: res[res.length-1].EndAddress-res[0].Address+1, data: null, timestamp: null}
                for (var row in res){
                  let tagRes = await this.db.execSql(`SELECT Address, DataType, Bit, WordSwapped, ByteSwapped FROM TagParams WHERE ID = ?`, [res[row].ID])
                  newPollgroups[pg.lastID].tags.push({
                    id:res[row].ID,
                    offset:(Number(tagRes[0].Address) -  Number(res[0].Address)) * 2 + Math.floor(tagRes[0].Bit/8), //byte offset = (tag address - pollgroup address)*2bytesPerReg +  
                    dataType: tagRes[0].DataType,
                    bit: Number(tagRes[0].Bit % 8),
                    wordSwapped: Number(tagRes[0].WordSwapped),
                    byteSwapped:  Number(tagRes[0].ByteSwapped)})
                  await this.db.runSql(`UPDATE TagParams
                  SET Pollgroup = ? WHERE ID = ?; `, [pg.lastID, res[row].ID]) 
                }     
              }
              start = end;
            }
          }
        }
        this.pollgroups = newPollgroups
      }
      catch(err){
        console.log(err)
      }
    
      this.optimizingActive = false;
    }

    this.readPollgroups = async ()=>{
      try{
        
        for(let g in this.pollgroups){
          let resp = null;
          let group = this.pollgroups[g]
          switch(group.function){
            case 1:
              resp = await this.connection.readCoils(Number(group.addr), Number(group.len));
              break;
            case 2:
              resp = await this.connection.readDiscreteInputs(Number(group.addr), Number(group.len));
              break;
            case 3:
              resp = await this.connection.readInputRegisters(Number(group.addr), Number(group.len));
              break;
            case 4:
              resp = await this.connection.readHoldingRegisters(Number(group.addr), Number(group.len))
              break;
          } 
          group.timestamp = Date.now()
          if(group.function == 3 || group.function == 4)
          {
            const buff = resp.response.body.valuesAsBuffer
            group.data = buff.swap16();//modbus registers are 16-bit Big-Endian.
          } else
          {
            group.data = resp.response.body.valuesAsArray
          }
          
        }
      }
      catch(err){
        console.log(err)
      }
    }

    this.updateTags = async ()=>{
      await this.writeToTags();
      try{
        if(this.optimizingActive){
          return null
        }
        let tagUpdates = {}
        await this.readPollgroups();
        for(let pg in this.pollgroups){
          for(let t in this.pollgroups[pg].tags){
            let tag = this.pollgroups[pg].tags[t]
            tagUpdates[tag.id] = {value: this.getTagValue(pg, t), timestamp: this.pollgroups[pg].timestamp}
          }
        }
        if(Object.keys(tagUpdates).length){
          //tagUpdates = {"46": {value: 9.99, timestamp: 150320402}, "85": {value: 10.99, timestamp: 150320402}}
          this.appManager.updateTagValues(tagUpdates)
        }
      }
      catch(err){
        console.log(err)
      }
    }

    this.getTagValue = (pg, t)=>{
      try{
        let group = this.pollgroups[pg] 
        let tag = group.tags[t]
        //console.table(tag)
        let val = null
        let buff = null
        if(group.data == null)
        {
          throw(Error(`Invalid poll group data for connection ${this.id}`))
        }
        switch(tag.dataType){
          case "BOOL":
            if(group.function == 3 || group.function == 4)
            {
              //offset is in bytes and tag.bit is the bit within the byte
              val = Boolean((group.data.readUInt8(tag.offset) & (1<<tag.Bit)));
            } else
            {
              //offset is the index of the array
              val = group.data[tag.offset];
            }
            break;
          case "USINT":
            buff = Buffer.alloc(1).fill(0);
            group.data.copy(buff, 0, tag.offset, tag.offset+1);
            val =  buff.readUInt8(0);
            break;
          case "SINT":
            buff = Buffer.alloc(1).fill(0);
            group.data.copy(buff, 0, tag.offset, tag.offset+1);
            val =  buff.readInt8(0);
            break;
          case "UINT":
            buff = Buffer.alloc(2).fill(0);
            group.data.copy(buff, 0, tag.offset, tag.offset+2);
            if(tag.byteSwapped){buff = buff.swap16();}
            val =  buff.readUInt16LE(0);
            break;
          case "INT":
            buff = Buffer.alloc(2).fill(0);
            group.data.copy(buff, 0, tag.offset, tag.offset+2);
            if(tag.byteSwapped){buff = buff.swap16();}
            val =  buff.readInt16LE(0);
            break;
          case "UDINT":
            buff = Buffer.alloc(4).fill(0);
            group.data.copy(buff, 0, tag.offset, tag.offset+4);
            if(tag.byteSwapped){buff = buff.swap16();}
            if(tag.wordSwapped){buff = buff.swap32();}
            val =  buff.readUInt32LE(0);
            break;
          case "DINT":
            buff = Buffer.alloc(4).fill(0);
            group.data.copy(buff, 0, tag.offset, tag.offset+4);
            if(tag.byteSwapped){buff = buff.swap16();}
            if(tag.wordSwapped){buff = buff.swap32();}
            val =  buff.readInt32LE(0);
            break;
          case "ULINT":
            buff = Buffer.alloc(8).fill(0);
            group.data.copy(buff, 0, tag.offset, tag.offset+8);
            if(tag.byteSwapped){buff = buff.swap16();}
            if(tag.wordSwapped){buff = buff.swap32();}
            val =  buff.readBigUInt64LE();
            break;
          case "LINT":
            buff = Buffer.alloc(8).fill(0);
            group.data.copy(buff, 0, tag.offset, tag.offset+8);
            if(tag.byteSwapped){buff = buff.swap16();}
            if(tag.wordSwapped){buff = buff.swap32();}
            val =  buff.readBigInt64LE(0);
            break;
          case "REAL":
            buff = Buffer.alloc(4).fill(0);
            group.data.copy(buff, 0, tag.offset, tag.offset+4);
            if(tag.byteSwapped){buff = buff.swap16();}
            if(tag.wordSwapped){buff = buff.swap32();}
            val =  buff.readFloatLE(0);
            break;
          case "LREAL":
            buff = Buffer.alloc(8).fill(0);
            group.data.copy(buff, 0, tag.offset, tag.offset+8);
            if(tag.byteSwapped){buff = buff.swap16();}
            if(tag.wordSwapped){buff = buff.swap32();}
            val =  buff.readDoubleLE(0);
            break;
          case "STRING":
            buff = Buffer.alloc(80).fill(0);
            group.data.copy(buff, 0, tag.offset, tag.offset+80);
            val = buff.toString('ascii')
            break;
          default:
            console.log(`Err: Unknown data type "${tag.dataType}"`)
          }
          return val
      }
      catch(err)
      {
        console.log(err);
      }
    }
    
    this.writeTag = async (tagWrite)=>{
      //{id: 1, value: "100.1"}
      try{
        let tagRes = await this.appManager.db.execSql(`
          SELECT * FROM ModbusTags WHERE ID = ? AND ConnectionID = ?;
        `, [tagWrite.id, this.id])
        if(tagRes.length){
          //this.encodeTagValue
          let tag = tagRes[0]
          let mask = 0x00000000;//used for single bit writes
          let val = 0x00000000;
          let resp; //use if the register value now is needed
          let buff = Buffer.alloc(8).fill(0);
          switch(true){
            case tag.DataType == "BOOL" && tag.Function == 4://need to read the register to set a single bit
              resp = await this.connection.readHoldingRegisters(Number(tag.Address), 1);
              mask = resp.response.body.valuesAsArray[0]; //read the register then set or clear the proper bit
              if(!Number(tagWrite.value)){
                val = mask & (0xFFFFFFFF ^ 1<<tag.Bit)
              }
              else{
                val = mask | 1<<tag.Bit
              }
              await this.connection.writeSingleRegister(tag.Address, val)
              break;
            case tag.DataType == "BOOL" && tag.Function == 1:
              await this.connection.writeMultipleCoils(tag.Address, [tagWrite.value]);//<<bug in single coil write, wtf?
              break;
            case tag.DataType == "SINT" && tag.Function == 4:
              //TODO
              break;
            case (tag.DataType == "USINT" && tag.Function == 4):
              //TODO
              break;
            case (tag.DataType == "INT" && tag.Function == 4):
              buff.writeInt16LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,2))
              break;
            case (tag.DataType == "UINT" && tag.Function == 4):
              buff.writeUInt16LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,2))
              break;
            case (tag.DataType == "DINT" && tag.Function == 4):
              buff.writeInt32LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              if(tag.WordSwapped){buff = buff.swap32();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,4))
              break;
            case (tag.DataType == "UDINT" & tag.Function == 4):
              buff.writeUInt32LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              if(tag.WordSwapped){buff = buff.swap32();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,4))
            case (tag.DataType == "LINT" && tag.Function == 4):
              buff.writeBigInt64LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              if(tag.WordSwapped){buff = buff.swap32();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,8))
              break;
            case (tag.DataType == "ULINT" && tag.Function == 4):
              buff.writeBigUInt64LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              if(tag.WordSwapped){buff = buff.swap32();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,8))
              break;
            case (tag.DataType == "REAL" && tag.Function == 4):
              buff.writeFloatLE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              if(tag.WordSwapped){buff = buff.swap32();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,4))
              break;
            case (tag.DataType == "LREAL" && tag.Function == 4):
              buff.writeDoubleLE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              if(tag.WordSwapped){buff = buff.swap32();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,8))
              break;
            default:
              console.log("Write function not impemented yet")
              break;            
          }
        }
        
      }
      catch(err){
        console.log(err)
      }
    }

    this.intiDb = async ()=>{
      try{
        await this.db.runSql(`PRAGMA foreign_keys = True;`)
        await this.db.runSql(`
        CREATE TABLE "TagParams" (
          "ID"	NUMERIC NOT NULL,
          "Function" INTEGER,
          "PollGroup"	INTEGER,
          "DataType" TEXT,
          "Address"	INTERGER NOT NULL,
          "EndAddress" INTEGER,
          "Bit"	BIT DEFAULT 0,
          "WordSwapped" BIT,
          "ByteSwapped" BIT,
          PRIMARY KEY("ID")
        );`)
        await this.db.runSql(`
        CREATE TABLE IF NOT EXISTS "PollGroups" (
          "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          "Function"	NUMERIC NOT NULL,
          "Address"	NUMERIC,
          "Length"	NUMERIC
          );`)
      }
      catch(err){
        this.appManager.log(err.message, err.stack)
      }
    }

    this.intiDb()
    /*
    functions = 0: coil (0xxxxx)
                1: input staus (1xxxxx)
                3: holding register (4xxxxx)
                4: input register (3xxxxx)
    */
    this.disconnect = async ()=>{
      this.socket.end();
    };
     

  }
}


class ModbusTCPConnection extends ModbusConnection{
  constructor(appManager, params){
    super(appManager, params)
    this.setup = ()=>
    {
      var check_list = ["Host", "Port", "StationID"];
      for (var param in check_list){
        if (params[check_list[param]] == undefined){
          throw("Incorrect params for ModbusTCP connection");
        }
      }
      
      this.socket = new net.Socket();
      this.connection = new Modbus.client.TCP(this.socket, params.StationID, 1000);
      this.Host = params.Host
      this.Port = params.Port
      this.StationID = params.StationID
      this.pollgroups = {}
      this.optimizingActive = false;
      this.socket.on('close', ()=>{
        this.emit('disconnected');
      })
      this.socket.on('connect', ()=> {
        this.emit('connected');
      })
      this.socket.on('error', (err)=>{
        var msg = `Modbus connection error in connection id ${this.id}: `;
        /* if (options.autoReconnect) {
          setTimeout(this.connect, 30000);
          msg += err.message + '\nRetrying in 30 seconds.'
        } */
        console.log(msg);  
      })
      this.on('tagSubUpdate', ()=>{this.optimizePollgroups()})
      this.connect = async ()=>{
        try{
          this.socket.connect({
            'host' : this.Host,
            'port' : this.Port,
            'autoReconnect': true})
        }
        catch(err){
          this.appManager.log(err.message, err.stack)
        }
      }
    }
    this.setup()
  }
}


class ModbusRTUConnection extends ModbusConnection{
  constructor(appManager, params){
    super(appManager, params)
    var check_list = ["Port", "StationID"];
    for (var param in check_list){
      if (params[check_list[param]] == undefined){
        throw("Incorrect params for ModbusRTU connection");
      }
    }
    this.Port = params.Port
    this.StationID = params.StationID
    this.socket = new SerialPort(this.Port, {baudRate: 19200}) //Need to add serial settings in database
    this.socket.on('open', ()=> {
      this.emit('connected');
    })
    this.socket.on('error', (err)=>{
      var msg = `Modbus connection error in connection id ${this.id}: `;
      /* if (options.autoReconnect) {
        setTimeout(this.connect, 30000);
        msg += err.message + '\nRetrying in 30 seconds.'
      } */
      console.log(msg);  
    })
    this.connection = new Modbus.client.RTU(this.socket, this.StationID)
    this.pollgroups = {}
    this.optimizingActive = false;
    this.socket.on('close', ()=>{
      this.emit('disconnected');
    })

    this.connect = async ()=>
    {
      //nothin here, serial port connects during instantiation
    }
    this.on('tagSubUpdate', ()=>{this.optimizePollgroups()})
  }
}


class GrblController{
  constructor(port)
  {
    this.port = port; //e.g. COM1
    this.portObj = null;
    //tags
    this.tags = {
      positionX:null,
      positionY:null,
      positionZ:null,
      overRidesFeed: null,
      overRidesRapid: null,
      overRidesSpindle: null,
      cooant: null, //0 = off, 1 = Flood, 2=Mist, 3=both??
      spindle: null, //0 = off, 1 = Fwd, 2=Rev
      state: null, //Idle, Run, Hold, Jog, Alarm, Door, Check, Home, Sleep
      pinX: null,
      pinY: null,
      pinZ:null,
      pinDoor: null,
      pinHold: null, 
      pinSoftReset: null,
      pinCycleStart: null,
      speedFeed: null,
      speedSpindle: null,
      buffBlocksIn: null,
      buffBytesAvailible: null,
      wcoX:null,
      wcoY:null,
      wcoZ:null
    }
    this.response = (line)=>
    {
      //pin updates only show up if true, init to false and if they are in param they will set to true
      machine.pin.x = false;
      machine.pin.y = false;
      machine.pin.z = false;
      machine.pin.door = false;
      machine.pin.hold = false;
      machine.pin.softReset = false;
      machine.pin.cycleStart = false;
      let params = line.replace(RegExp(`[<]`, 'g'), '').replace(RegExp(`[>]`, 'g'), '').split('|')
      let vals;
      for(let idx=0;idx< params.length; idx++)
      {
        let val = params[idx]
        switch(true){
          case(idx==0):
            machine.state = val;
            break;
          case(val.startsWith('MPos')):
            vals = val.replace(RegExp(`[MPos:]`, 'g'), '').split(',');
            machine.position.x = Number(vals[0]);
            machine.position.y = Number(vals[1]);
            machine.position.z = Number(vals[2]);
            break;
          case(val.startsWith('Bf')):
            vals = val.replace(RegExp(`[Bf:]`, 'g'), '').split(',');
            machine.buffer.blocksIn = Number(vals[0]);
            machine.buffer.bytesAvailible = Number(vals[1]);
            break;
          case(val.startsWith('FS')):
            vals = val.replace(RegExp(`[FS:]`, 'g'), '').split(',');
            machine.speed.feed = Number(vals[0]);
            machine.speed.spindle = Number(vals[1]);
            break;
          case(val.startsWith('Ov')):
            vals = val.replace(RegExp(`[Ov:]`, 'g'), '').split(',');
            machine.overRides.feed = Number(vals[0]);
            machine.overRides.rapid = Number(vals[1]);
            machine.overRides.spindle = Number(vals[2]);
            break;
          case(val.startsWith('WCO')):
            vals = val.replace(RegExp(`[WCO:]`, 'g'), '').split(',');
            machine.workCoordinateOffset.x = Number(vals[0]);
            machine.workCoordinateOffset.y = Number(vals[1]);
            machine.workCoordinateOffset.z = Number(vals[2]);
            break;
          case(val.startsWith('Pn')):
            vals = val.replace(RegExp(`[Pn:]`, 'g'), '');
            machine.pin.x = (vals.indexOf('X') >= 0);
            machine.pin.y = (vals.indexOf('Y') >= 0);
            machine.pin.z = (vals.indexOf('Z') >= 0);
            machine.pin.doorHold = (vals.indexOf('D') >= 0);
            machine.pin.hold = (vals.indexOf('H') >= 0);
            machine.pin.softReset = (vals.indexOf('R') >= 0);
            machine.pin.cycleStart = (vals.indexOf('S') >= 0);
            break;
          case(val.startsWith('A')):
            vals = val.replace(RegExp(`[A:]`, 'g'), '');
            machine.spindle = (2 * (vals.indexOf('C') >= 0)) + (vals.indexOf('S') >= 0);
            machine.cooant = (2 * (vals.indexOf('M') >= 0)) + (vals.indexOf('F') >= 0);
            break;
          default:
            throw(Error(`unhandled parameter in grbl parse: ${val}`))
          }
        }
    }

    this.connect = async ()=>
    {
      try
      {
        this.portObj = new SerialPort(this.port, { baudRate: 115200 })
        portObj.pipe(parser);
      }
      catch(err)
      {
        console.log(err);
      }
    }

    this.writeTag = async (tagWrite)=>
    {
      try{
        throw(Error(`writeTag method not implented for this connection type`))
      }
      catch(err){
        console.log(err)
      }
    }

    this.updateTags = async ()=>
    {
      await this.writeToTags();
      try
      {
        let tagUpdates = {}
        for(let tag in handles){
          if (handles[h].err) {
            console.log(`Error: ${handles[h].symname}; ${ads.ERRORS[handles[h].err]}`)
          } 
          else {
            tagUpdates[handles[h].tagId] ={
              value: handles[h].value,
              timestamp: Date.now()}
            
          }
        }
        if(Object.keys(tagUpdates).length)
        {
          //tagUpdates = {"46": {value: 9.99, timestamp: 150320402}, "85": {value: 10.99, timestamp: 150320402}}
          this.appManager.updateTagValues(tagUpdates)
        }
      }
      catch(err)
      {
        console.log(err)
      }
    }
  }
}


class GrblConnection extends Connection{
  constructor(appManager, params)
  {
    super(appManager, params)
    this.port = params.Port;
    this.pollrate = params.Pollrate;
    this.pollObj = null;
    this.controller = null;
    this.on('tagSubUpdate', ()=>{})
    const cls = this;
    this.connect = async ()=>{
      try
      {
        cls.controller = new GrblController(this.port);
        await cls.controller.connect();
        cls.emit('connected')
        setInterval(this.pollLoop, this.pollrate)
        cls.controller.on('update', ()=>{})
      }
      catch(err)
      {
        console.log(`Trouble connecting to connection ID ${this.id}, Grbl controller @ ${this.host}. ${err}`)
      }
    }
  }
}
/*
const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const lineReader = require('line-reader');
const parser = new Readline()
const port = new SerialPort('COM6', { baudRate: 115200 })
port.pipe(parser);

//gloabals

let gCodeLines = [];
const grblRxSz = 128
let grblBuffer = {charsWaiting: 0,
                  linesSent: []}

const machine = {
  position: {x:null, y:null, z:null},
  overRides: {feed: null, rapid: null, spindle: null},
  cooant: null, //0 = off, 1 = Flood, 2=Mist, 3=both??
  spindle: null, //0 = off, 1 = Fwd, 2=Rev
  state: null,
  pin: {x: null, y: null, z:null, door: null, hold: null, softReset: null, cycleStart: null},
  speed: {feed: null, spindle: null},
  buffer: {blocksIn: null, bytesAvailible: null},
  workCoordinateOffset: {x:null, y:null, z:null},
}




const update = (line)=>
{
  //pin updates only show up if true, init to false and if they are in param they will set to true
  machine.pin.x = false;
  machine.pin.y = false;
  machine.pin.z = false;
  machine.pin.door = false;
  machine.pin.hold = false;
  machine.pin.softReset = false;
  machine.pin.cycleStart = false;
  let params = line.replace(RegExp(`[<]`, 'g'), '').replace(RegExp(`[>]`, 'g'), '').split('|')
  params.forEach((val,idx)=>
  {
    let vals;
    switch(true){
      case(idx==0):
        machine.state = val;
        break;
      case(val.startsWith('MPos')):
        vals = val.replace(RegExp(`[MPos:]`, 'g'), '').split(',');
        machine.position.x = Number(vals[0]);
        machine.position.y = Number(vals[1]);
        machine.position.z = Number(vals[2]);
        break;
      case(val.startsWith('Bf')):
        vals = val.replace(RegExp(`[Bf:]`, 'g'), '').split(',');
        machine.buffer.blocksIn = Number(vals[0]);
        machine.buffer.bytesAvailible = Number(vals[1]);
        break;
      case(val.startsWith('FS')):
        vals = val.replace(RegExp(`[FS:]`, 'g'), '').split(',');
        machine.speed.feed = Number(vals[0]);
        machine.speed.spindle = Number(vals[1]);
        break;
      case(val.startsWith('Ov')):
        vals = val.replace(RegExp(`[Ov:]`, 'g'), '').split(',');
        machine.overRides.feed = Number(vals[0]);
        machine.overRides.rapid = Number(vals[1]);
        machine.overRides.spindle = Number(vals[2]);
        break;
      case(val.startsWith('WCO')):
        vals = val.replace(RegExp(`[WCO:]`, 'g'), '').split(',');
        machine.workCoordinateOffset.x = Number(vals[0]);
        machine.workCoordinateOffset.y = Number(vals[1]);
        machine.workCoordinateOffset.z = Number(vals[2]);
        break;
      case(val.startsWith('Pn')):
        vals = val.replace(RegExp(`[Pn:]`, 'g'), '');
        machine.pin.x = (vals.indexOf('X') >= 0);
        machine.pin.y = (vals.indexOf('Y') >= 0);
        machine.pin.z = (vals.indexOf('Z') >= 0);
        machine.pin.doorHold = (vals.indexOf('D') >= 0);
        machine.pin.hold = (vals.indexOf('H') >= 0);
        machine.pin.softReset = (vals.indexOf('R') >= 0);
        machine.pin.cycleStart = (vals.indexOf('S') >= 0);
        break;
      case(val.startsWith('A')):
        vals = val.replace(RegExp(`[A:]`, 'g'), '');
        machine.spindle = (2 * (vals.indexOf('C') >= 0)) + (vals.indexOf('S') >= 0);
        machine.cooant = (2 * (vals.indexOf('M') >= 0)) + (vals.indexOf('F') >= 0);
        break;
      default:
        throw(Error(`unhandled parameter in grbl parse: ${val}`))
    }
  })
  //console.log(JSON.stringify(machine.buffer, null, 2))
}

const readGcode = ()=>
{
  gCodeLines = []
  lineReader.eachLine('C:/Users/asolchenberger/Documents/Fusion/testtech.nc', (line)=>
  {
    gCodeLines.push(`${line}\n`);
  })
}
setTimeout(readGcode, 1000)



const writeGcode = ()=>
{
  if(grblBuffer.linesSent.length)
  {//0 line has been proccessed, remove form buffer
    grblBuffer.linesSent.shift()
    
  }
  grblBuffer.charsWaiting = 0; //init to recount
  for(let x=0;x<grblBuffer.linesSent.length;x++)
  {
    grblBuffer.charsWaiting += grblBuffer.linesSent[x].length;
  }
  while((grblRxSz-grblBuffer.charsWaiting) >= gCodeLines[0].length)
    {
      let l = gCodeLines.shift()
      grblBuffer.linesSent.push(l)//shift line from start of gCode and push to the sent list
      console.log(l)
      port.write(l)
      grblBuffer.charsWaiting += l.length
    }
    console.log(JSON.stringify(machine, null, 2))
}
setTimeout(writeGcode, 5000)
setInterval(()=>{port.write('?')}, 100);

parser.on('data', (line)=>
{
  if(line.startsWith('<'))
  {
    update(line);
  }
  if((line.startsWith('ok')) && gCodeLines.length)
    {

      writeGcode(); 
    }
})
port.write('?');

*/














class TagSubscriptions extends EventEmitter{
  constructor(appManager){
    super();
    this.appManager = appManager
    this.addSubscription = async (tagIds)=>{
      try
        {//each subsciption gets an ID back from inserting a Subscription.
        let res = await this.appManager.db.runSql(`INSERT INTO TagSubscriptions VALUES (NULL)`)
        let subId = res.lastID
        this.changeSubscription(subId,tagIds)
        return subId}
      catch(err){
        console.log(err.message, err.stack)
      }
    }

    this.removeSubscription = async (subId, listener)=>{
      try
        {await this.appManager.db.runSql(`DELETE FROM "TagSubscriptions" WHERE SubscriptionID = ?`, [subId]).catch(err=>console.log(err))
        this.removeListener(listener)
      }
      catch(err){
        console.log(err.message, err.stack)
      }
    }

    this.changeSubscription = async (subId, tagIds) =>{
      try{
        let subCheck = await this.appManager.db.execSql(`SELECT (ID) FROM TagSubscriptions WHERE ID = ?;`, [subId])
        if(!subCheck.length){
          return
        }
        await this.appManager.db.runSql(`DELETE FROM "TagSubRelations" WHERE SubID = ?`, [subId])//remove the old sub-tag rows
        .catch(err=>console.log(err))
        
        //console.table(tagRows)
        for(let t in tagIds){
          let tagCheck = await this.appManager.db.execSql(`SELECT (ID) FROM Tags WHERE ID = ?;`, [tagIds[t]])
          if(tagCheck.length){//if this tag is in the Tags table
            await this.appManager.db.runSql(`INSERT INTO TagSubRelations (SubID, TagID) VALUES (?,?)`, [subId, tagIds[t]])
          }
        }
        //emit the connection ids of all connections involved
        let connRes = await this.appManager.db.execSql(`SELECT DISTINCT Tags.ConnectionID FROM TagSubRelations 
        INNER JOIN Tags ON Tags.ID = TagSubRelations.TagID 
        WHERE TagSubRelations.SubID = ?;`, [subId])
        for(let row in connRes){
          this.appManager.emit('TagSubscriptionChanged', connRes[row].ConnectionID)
        }
      }
      catch(err){
        console.log(err.message + err.stack)
      }
    }

    this.getSubscriptionTags = async (connId)=>{
      //gather all tags this connection need to poll
      try{
        let res = await this.appManager.db.execSql(`
        SELECT DISTINCT TagSubRelations.TagID FROM TagSubRelations 
          INNER JOIN Tags ON Tags.ID = TagSubRelations.TagID 
          WHERE Tags.ConnectionID = ?;`, [connId])
          const tags = []
          for(let row in res){
            tags.push(res[row].TagID)
          }
          return tags}
      catch(err){
        console.log(err)
      }
    }
    

    this.updateConnections = async (connectionIds)=>{
      //call this when the connected status of a connection changes
      //if it exists in the appManager.connections it assumes it's connected
      try{
        connectionIds.map(async (id)=>{
          //get the tags in subs with this id
          let tagRows = await this.appManager.db.execSql(`SELECT ID FROM Tags INNER JOIN TagSubRelations ON Tags.ID = TagSubRelations.TagID WHERE Tags.ConnectionID = ?;`, [id])
          .catch(err=>console.log(err))
          if(!this.appManager.connections[id]){ //if connection not open
            let ts = Date.now()
            this.appManager.updateTagValues(tagRows.map((tagRow)=>{
              return {tagId: tagRow.ID, value: null, timestamp: ts}
            }))//set tag values to bad quality
          }
        })
        
      }
      catch(err){
        console.log(err.message, err.stack)
      }
    }

  }
}

module.exports = {
  SimulatedConnection,
  AdsConnection,
  ModbusRTUConnection,
  ModbusTCPConnection,
  EthernetIPConnection,
  GrblConnection,
  TagSubscriptions}



               