# HTML-HMI
## Intro
### What is HTML-HMI?
The [HTML-HMI](https://bitbucket.org/testtechsolutions/html-hmi) is an open-source project to provide a human-machine interface for industrial controllers using widly availible technoligies. The project uses the  [Electron](https://electronjs.org/) frame work as a to provide a modern programming interface to the desktop. [Electron](https://electronjs.org/) is a project that combines the [Node.js](https://nodejs.org/) framework and the [Chromium](https://www.chromium.org/) project to create a desktop application that is basically a webserver and webbrowser in a single application. The goal of the [HTML-HMI](https://bitbucket.org/testtechsolutions/html-hmi) is to provide a basis for a profesional, industrial HMI system. 
### How does it work?
The core functionality of the application is to provide the visual interface "Widgets" value information related to the system that the application is communicating with. This is mostly done on a periodic interval configured on each connection of the application. Another key feature of the application is the central database. This databse contains all of the information the application need to connect and gather information for the datapoints ("Tags") in the application. The application uses a subcription-based model to gather data. This means network traffic is optimized, and prevents exessive network calls. The application also brings together many open-source industrial protocol libraries.  
### What can this be used for?
Basically any data-driven interface. Since the backend is written for [Node.js](https://nodejs.org/), there are endless libraries availible. The frontend uses HTML5, CSS, and JavaScript so, again, there are endless resouces availible for the any visual interface one could build. The goal of the project is aimed toward an industrial interface, but the following are examples of for the application in any combination:
* Example Uses
  * Real-time Industrial Interfaces
    * Water Treatment
    * SCADA Interface
    * Production Floor
    * Power Plant
  * Farm and Building Automation
    * Boiler System
    * Irrigation
    * Security
    * Remote Operation
  * Cyclically refreshed
    * Web Queries
    * Database Queries

The application features are documented below
## Table of Contents

* [AppManager](./classes/AppManager.md)