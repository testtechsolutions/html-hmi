
const font = `"Helvetica"`

class WidgetFactory{
  constructor(doc, windowEmitter){
    this.doc = doc;
    this.parent = undefined;
    this.emitter = windowEmitter;
    this.widgets = {
      toggleLed: ToggleLed,
      valueDisplay: ValueDisplay,
      boolIndicator: BoolIndicator,
      multistate: MultiState,
      mod_valueDisplay: Mod_ValueDisplay

      }
    this.tagSubs = []
    this.addWidget = (wParams)=>{
      if(this.widgets[wParams.widget]== undefined){
        console.log(`Unknow widget type: ${wParams.widget}`)
        return
      }
      //console.log(wParams)
      wParams.doc = this.doc;
      wParams.parent = this.parent;
      wParams.emitter = this.emitter;
      //console.log(wParams)
      const w = new this.widgets[wParams.widget](wParams)
      this.tagSubs.push(...w.getSubscriptionTags())
      //TODO gather subscription data here 
    }
    this.build = (cfg)=>{
      if(this.parent === undefined){//on first lookup, hold a ref to the content div
        this.parent = this.doc.getElementById("content");
      }
      this.tagSubs = [] //clear the subs
      this.parent.innerHTML = "";
      //cfg is an object of widgets
      for(let w in cfg.widgets){
        let wParams = cfg.widgets[w]
        this.addWidget(wParams)
      }
      requestTagIds({window: "MAIN", tags:this.tagSubs})
    }
  }
}

class Widget{
  constructor(params){ //pass in the document
    this.doc = params.doc;
    this.parent = params.parent
    this.getSubscriptionTags = ()=>{
      //return an array of tag ids this widget need to be updated with
      //override this for each widget
      //the page iterates over all widgets to get tags to subscribe to
      return []; 
    }
    this.updateTags = (tagVals)=>{
      //override this to update the widget's tags
    }
    this.updateTagIds = (connectionDef)=>{
      //override this to update the widget's tags
    }

    let cls = this;
    params.emitter.on('TagSubscriptionUpdate', (tagVals)=>{cls.updateTags(tagVals)
    })
    params.emitter.on('returnTagIds', (connectionDef)=>{cls.updateTagIds(connectionDef)
    })
  }

}

class ToggleLed extends Widget{
  constructor(params){//doc, parent, x, y, id,
    super(params);
    this.readTagId = params.readTagId;
    this.writeTagId = params.writeTagId;
    this.elementRefs = {
      div: this.doc.createElement("div")
    }
    this.parent.appendChild(this.elementRefs.div)
    this.elementRefs.div.id = params.id
    this.elementRefs.div.style = `
    width: 200px; height: 200px;
    position: fixed;
    top:${params.y}px;
    left:${params.x}px;`
    this.elementRefs.div.innerHTML = `
        <svg width="200" height="200">
        <path fill="#AAA" d="M40,5 V50 H160 V5 H40 " stroke="#000" />
        <path fill="#AAA" d="M40,195 V150 H160 V195 H40 " stroke="#000" />
          <circle id="${params.id}-circle" stroke="black" stroke-width="4" fill="green" cx="100" cy="100" r="50"/>
          <text id="${params.id}-label" color="#ffffff" text-anchor="middle" font-size="32" font-family=${font} x="100" y="40">${params.label}</text>
          <text id="${params.id}-value" color="#ffffff" text-anchor="middle" font-size="32" font-family=${font} x="100" y="184">Off</text>
          <path fill="none" d="M5,5 H195 V195 H5 V5 M40,5 V90 Q0,100 40,110 V195 M160,5 V90 Q200,100 160,110 V195" stroke="#000" />
        </svg>`//
    this.elementRefs.circle = document.getElementById(`${params.id}-circle`)
    this.elementRefs.value = document.getElementById(`${params.id}-value`)
    this.elementRefs.label = document.getElementById(`${params.id}-label`)
    this.clicked = ()=>{
              
    }
    this.elementRefs.circle.onclick = this.clicked
    this.updateTags = (tagVals)=>{//{1:{value: 3.14, timestamp: 123132123}, 2:{value: 3.14, timestamp: 123132123}}
      let val = false //default
      if(tagVals[this.readTagId] != undefined){
        val = (tagVals[this.readTagId].value > 5.0)
        this.elementRefs.value.innerHTML = Number.parseFloat(tagVals[this.readTagId].value).toFixed(3)
      if(val){
          this.elementRefs.circle.setAttribute('fill', 'red')   
          
      }
      else{
          this.elementRefs.circle.setAttribute('fill', 'green')
      }
      
      }
    }
    this.getSubscriptionTags = ()=>{
      return [this.readTagId]; //return all the tagIds needed for updating this widget
    }
  }
}

class ValueDisplay extends Widget{
  constructor(params){
    super(params);
    this.readTagId = params.readTagId;
    this.readTag = params.readTag
    this.elementRefs = {
      div: this.doc.createElement("div")
    }
    this.parent.appendChild(this.elementRefs.div)
    this.elementRefs.div.id = params.id
    if(params.onclick){      
      const clicker = this.doc.createElement('button')
      clicker.style = `width: 200px; height: 100px;
      position: fixed;
      top:${params.y}px;
      left:${params.x}px;
      z-index: 100;
      background-color: #0000;
      border: 2px solid #000`
      clicker.onclick = eval(`()=>{${params.onclick}}`)
      this.parent.appendChild(clicker)
    }
    this.elementRefs.div.style = `
    width: 200px; height: 100px;
    position: fixed;
    top:${params.y}px;
    left:${params.x}px;`
    this.elementRefs.div.innerHTML = `
    <svg width="200" height="100">
    <pattern id="${params.id}-fillPattern" patternUnits="userSpaceOnUse" width="4" height="4">
    <path id="${params.id}-badQual" d="M-1,1 l2,-2
             M0,4 l4,-4
             M3,5 l2,-2" 
          style="stroke:#AAAF; stroke-width:1" />
  </pattern>
        <rect fill= "#AAA" x="5" y="5" width="190" height="90" rx="10" ry="10" stroke="#000" stroke-width="4"/>
        <path stroke="#000" stroke-width="4" d="M5,50h190"/>
        <text color="#ffffff" text-anchor="middle" font-size="24" font-family=${font} x="100" y="30">${params.label}</text>
        <rect fill="#000" x="20" y="55" width="160" height="35" rx="10" ry="10" stroke="#000"/>
        <text id="${params.id}-value" fill="#0F0" text-anchor="middle" font-size="24" font-family=${font} x="100" y="80">??.??</text>
        <rect fill="url(#${params.id}-fillPattern)" x="20" y="55" width="160" height="35" rx="10" ry="10" stroke="#000"/>
    </svg>`
    this.elementRefs.value = document.getElementById(`${params.id}-value`)
    this.elementRefs.badQual = document.getElementById(`${params.id}-badQual`)

    this.updateTags = (tagVals)=>{//{1:{value: 3.14, timestamp: 123132123}, 2:{value: 3.14, timestamp: 123132123}}
      if(tagVals[this.readTagId] != undefined){
        this.elementRefs.badQual.setAttribute("style", "stroke:#AAA0");
        this.elementRefs.value.innerHTML = Number(tagVals[this.readTagId].value).toFixed(3)
      }
      else{
        this.elementRefs.badQual.setAttribute("style", "stroke:#AAAF");
        this.elementRefs.value.innerHTML = "??.??";
      }
     }
    this.updateTagIds = (reply)=>{
      if (reply.connections == undefined){
        return
      }
      let connectionDef = reply.connections
      //if defined the id will be set
      if(connectionDef[this.readTag.connection] === undefined){ return }
      if(connectionDef[this.readTag.connection][this.readTag.tag] === undefined){ return }
      this.readTagId = connectionDef[this.readTag.connection][this.readTag.tag];
    }

    this.getSubscriptionTags = ()=>{
      return [this.readTag]; //return all the tagIds needed for updating this widget
    }
  }
}

class BoolIndicator extends Widget{
  constructor(params){
    super(params);
    this.readTagId = params.readTagId;
    this.readTag = params.readTag
    this.elementRefs = {
      div: this.doc.createElement("div")
    }
    this.parent.appendChild(this.elementRefs.div)
    this.elementRefs.div.id = params.id
    this.elementRefs.div.style = `
    width: 200px; height: 50px;
    position: fixed;
    top:${params.y}px;
    left:${params.x}px;`
    //<rect id="${params.id}-ind" fill= ${params.colors.err} x="5" y="5" width="40" height="40" rx="4" ry="4" stroke="#000" stroke-width="4"/>
    this.elementRefs.div.innerHTML = `
    <svg width="200" height="50">
        <circle id="${params.id}-ind" fill= ${params.colors.err} cx="25" cy="25" r="20" stroke="#000" stroke-width="4"/>
        <text color="#ffffff" text-anchor="left" font-size="24" font-family=${font} x="50" y="35">${params.label}</text>
    </svg>`
    this.elementRefs.ind = document.getElementById(`${params.id}-ind`)

    this.updateTags = (tagVals)=>{//{1:{value: 3.14, timestamp: 123132123}, 2:{value: 3.14, timestamp: 123132123}}
      if(tagVals[this.readTagId] != undefined){
        this.elementRefs.ind.setAttribute("fill", params.colors.err);
      }
      else{
        let color = (tagVals[this.readTagId]) ? params.colors.t : params.colors.f; 
        this.elementRefs.ind.setAttribute("fill", color);
      }
     }
    this.updateTagIds = (reply)=>{
      if (reply.connections == undefined){
        return
      }
      let connectionDef = reply.connections
      //if defined the id will be set
      if(connectionDef[this.readTag.connection] === undefined){ return }
      if(connectionDef[this.readTag.connection][this.readTag.tag] === undefined){ return }
      this.readTagId = connectionDef[this.readTag.connection][this.readTag.tag];
    }

    this.getSubscriptionTags = ()=>{
      return [this.readTag]; //return all the tagIds needed for updating this widget
    }
  }
}




class MultiState extends Widget{
  constructor(params){
    super(params);
    this.readTagId = params.readTagId;
    this.states = params.states
    this.readTag = params.readTag
    this.elementRefs = {
      div: this.doc.createElement("div")
    }
    this.parent.appendChild(this.elementRefs.div)
    this.elementRefs.div.id = params.id
    if(params.onclick){      
      const clicker = this.doc.createElement('button')
      clicker.style = `width: 200px; height: 100px;
      position: fixed;
      top:${params.y}px;
      left:${params.x}px;
      z-index: 100;
      background-color: #0000;
      border: 2px solid #000`
      clicker.onclick = eval(`()=>{${params.onclick}}`)
      this.parent.appendChild(clicker)
    }
    this.elementRefs.div.style = `
    width: 200px; height: 100px;
    position: fixed;
    top:${params.y}px;
    left:${params.x}px;`
    this.elementRefs.div.innerHTML = `
    <svg width="200" height="100">
    <pattern id="${params.id}-fillPattern" patternUnits="userSpaceOnUse" width="4" height="4">
    <path id="${params.id}-badQual" d="M-1,1 l2,-2
             M0,4 l4,-4
             M3,5 l2,-2" 
          style="stroke:#AAAF; stroke-width:1" />
  </pattern>
        <rect fill= "#AAA" x="5" y="5" width="190" height="90" rx="10" ry="10" stroke="#000" stroke-width="4"/>
        <path stroke="#000" stroke-width="4" d="M5,50h190"/>
        <text color="#ffffff" text-anchor="middle" font-size="24" font-family=${font} x="100" y="30">${params.label}</text>
        <rect fill="#000" x="20" y="55" width="160" height="35" rx="10" ry="10" stroke="#000"/>
        <text id="${params.id}-value" fill="#0F0" text-anchor="middle" font-size="24" font-family=${font} x="100" y="80">??.??</text>
        <rect fill="url(#${params.id}-fillPattern)" x="20" y="55" width="160" height="35" rx="10" ry="10" stroke="#000"/>
    </svg>`
    this.elementRefs.value = document.getElementById(`${params.id}-value`)
    this.elementRefs.badQual = document.getElementById(`${params.id}-badQual`)

    this.updateTags = (tagVals)=>{//{1:{value: 3.14, timestamp: 123132123}, 2:{value: 3.14, timestamp: 123132123}}
      if(tagVals[this.readTagId] != undefined){
        this.elementRefs.badQual.setAttribute("style", "stroke:#AAA0"); //set quality to good
        const val = Math.floor(tagVals[this.readTagId].value); //trunc to integer
        this.elementRefs.value.innerHTML = this.states[val];
      }
      else{
        this.elementRefs.badQual.setAttribute("style", "stroke:#AAAF");
        this.elementRefs.value.innerHTML = "??.??";
      }
     }
    this.updateTagIds = (reply)=>{
      if (reply.connections == undefined){
        return
      }
      let connectionDef = reply.connections
      //if defined the id will be set
      if(connectionDef[this.readTag.connection] === undefined){ return }
      if(connectionDef[this.readTag.connection][this.readTag.tag] === undefined){ return }
      this.readTagId = connectionDef[this.readTag.connection][this.readTag.tag];
    }

    this.getSubscriptionTags = ()=>{
      return [this.readTag]; //return all the tagIds needed for updating this widget
    }
  }
}

class Mod_ValueDisplay extends Widget{
  constructor(params){
    super(params);
    this.readTagId = params.readTagId;
    this.readTag = params.readTag
    this.elementRefs = {
      div: this.doc.createElement("button"),
      container:this.doc.createElement("div"),
      lb1:this.doc.createElement("label"),
      lb2 :this.doc.createElement("label2")
    }
    this.parent.appendChild(this.elementRefs.div)
    this.elementRefs.div.id = params.id
    this.elementRefs.div.className = 'widget'

    this.elementRefs.div.style = `
    top:${params.y}px;
    left:${params.x}px;`
    //this.elementRefs.div.visibility= 'visible'
    //this.elementRefs.divstyle.left = `${left}px`;
    //this.elementRefs.divtop = `${top}px`;
    //this.elementRefs.div.innerHTML = 6
    //let container = this.doc.createElement("div")
    //let lb1 = this.doc.createElement("label")
    this.elementRefs.lb1.innerHTML = params.label
    //let lb2 = this.doc.createElement("label2")
    this.elementRefs.lb2.innerHTML = '??.??'
    //console.log('aslkjf',this.elementRefs.div.children)
    //this.elementRefs.div.childNodes.div.childNodes.lab2.innerHTML = "??.??";
    this.elementRefs.div.appendChild(this.elementRefs.container)
    if (params.setup.inverted){
      this.elementRefs.container.appendChild(this.elementRefs.lb2)
      this.elementRefs.container.appendChild(this.elementRefs.lb1)
    }else{
      this.elementRefs.container.appendChild(this.elementRefs.lb1)
      this.elementRefs.container.appendChild(this.elementRefs.lb2)
    }
    console.log(this.elementRefs.div)
    if(params.setup.orientation == 'horizontal'){
      this.elementRefs.div.style.width = "400px"
      this.elementRefs.div.style.height = "40px"
    }else{
      this.elementRefs.div.style.width = "200px"
      this.elementRefs.lb1.style.display = "block"
      this.elementRefs.lb2.style.display = "block"
    }

    //this.elementRefs.lb2.style.strokeWidth = "4"
    //this.elementRefs.lb2.style.backgroundImage = "repeating-linear-gradient(45deg red blue)"
    //console.log('hmmm',this.elementRefs.lb2.style.backgroundImage)
    //background-image: repeating-linear-gradient(45deg, red, blue 7%, green 10%);
    //this.elementRefs.lb2.setAttribute("style", "stroke:#AAA0");


/*     `<svg width="200" height="100">
    <pattern id="${params.id}-fillPattern" patternUnits="userSpaceOnUse" width="4" height="4">
    <path id="${params.id}-badQual" d="M-1,1 l2,-2
             M0,4 l4,-4
             M3,5 l2,-2" 
          style="stroke:#AAAF; stroke-width:1" />
  </pattern>
        <rect fill= "#AAA" x="5" y="5" width="190" height="90" rx="10" ry="10" stroke="#000" stroke-width="4"/>
        <path stroke="#000" stroke-width="4" d="M5,50h190"/>
        <text color="#ffffff" text-anchor="middle" font-size="24" font-family=${font} x="100" y="30">${params.label}</text>
        <rect fill="#000" x="20" y="55" width="160" height="35" rx="10" ry="10" stroke="#000"/>
        <text id="${params.id}-value" fill="#0F0" text-anchor="middle" font-size="24" font-family=${font} x="100" y="80">??.??</text>
        <rect fill="url(#${params.id}-fillPattern)" x="20" y="55" width="160" height="35" rx="10" ry="10" stroke="#000"/>
    </svg>` */
    //this.elementRefs.value = document.getElementById(`${params.id}-value`)
    //this.elementRefs.badQual = document.getElementById(`${params.id}-badQual`)

    this.updateTags = (tagVals)=>{//{1:{value: 3.14, timestamp: 123132123}, 2:{value: 3.14, timestamp: 123132123}}
      console.log('tagVals',tagVals)
      if(tagVals[this.readTagId] != undefined){
        this.elementRefs.badQual.setAttribute("style", "stroke:#AAA0");
        this.elementRefs.lb2.innerHTML = Number(tagVals[this.readTagId].value).toFixed(3)
      }
      else{
        this.elementRefs.badQual.setAttribute("style", "stroke:#AAAF");
        this.elementRefs.lb2.innerHTML = "??.??";
      }
     }
    this.updateTagIds = (reply)=>{
      if (reply.connections == undefined){
        return
      }
      let connectionDef = reply.connections
      //if defined the id will be set
      if(connectionDef[this.readTag.connection] === undefined){ return }
      if(connectionDef[this.readTag.connection][this.readTag.tag] === undefined){ return }
      this.readTagId = connectionDef[this.readTag.connection][this.readTag.tag];
    }

    this.getSubscriptionTags = ()=>{
      return [this.readTag]; //return all the tagIds needed for updating this widget
    }
  }
}


module.exports = {WidgetFactory}